resource "google_compute_firewall" "fw-externalssh-all" {
  name    = "fw-externalssh-all"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["22", "9000"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["${var.target_tags}"]
}
